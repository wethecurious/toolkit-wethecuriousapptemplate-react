import * as React from 'react'
import { HelloWorld } from '../../components/HelloWorld'

export const Hello: React.SFC = () => (
  <>
    <HelloWorld />
  </>
)
