import * as React from 'react'
import { Text, Heading } from '@wethecurious/wtc-ui-library'
import { Link } from 'react-router-dom'
import { Path } from '../../utils/pathsEnum'

export const Home: React.SFC = () => (
  <>
    <Heading>Welcome</Heading>
    <Text>We The Curious</Text>
    <Link to={Path.Hello}>Hello</Link>
  </>
)
