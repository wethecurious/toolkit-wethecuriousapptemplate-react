import * as React from 'react'

interface ProvidedAppProps {
  theme: any
}

interface AsyncThemeState {
  theme: any
  error?: any
}

export const asyncTheme = (WrappedComponent: React.SFC<ProvidedAppProps> | React.ComponentClass<ProvidedAppProps>, url: string): React.ComponentClass<{}, AsyncThemeState> => (
  class extends React.Component<{}, AsyncThemeState> {
    constructor(props: ProvidedAppProps) {
      super(props)
      this.state = {
        theme: undefined,
      }
    }

    componentDidMount() {
      fetch(url)
        .then(theme => theme.json())
        .then(theme => this.setState({ theme }))
        .catch(error => this.setState({ error }))
    }

    render() {
      return this.state.theme ?
        <WrappedComponent theme={this.state.theme}/>
        : <h1>Start theme server</h1>
    }
  }
)
