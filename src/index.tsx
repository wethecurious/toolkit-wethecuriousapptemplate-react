import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { App } from './App'
import { WTCProvider } from '@wethecurious/wtc-ui-library'
import { asyncTheme } from './hoc/asyncTheme'
import { ThemeProvider } from 'styled-components'
import { store } from './reducers/store/reduce'
import './global.css'

interface Props {
  theme: any
}

const Container = (props: Props) => (
  <WTCProvider theme={props.theme}>
    <ThemeProvider theme={props.theme}>
      <Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Provider>
    </ThemeProvider>
  </WTCProvider>
)

const ThemedContainer = asyncTheme(Container, 'https://assets.wethecurious.io/1/themes/latest')

ReactDOM.render(
<ThemedContainer />,
  document.getElementById('app')
)
