import { connect } from 'react-redux'
import { StoreState } from '../../reducers/store/storeState'
import { HelloWorldComponent } from './HelloWorldComponent'
import { Dispatch } from 'redux'
import { setDemoMessage } from '../../reducers/demoReducer'

const mapStateToProps = (state: StoreState) => ({
  demoMessage: state.demoReducer.demoMessage,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
  setDemoMessage: (message: string) => dispatch(setDemoMessage(message)),
})

export const HelloWorld = connect(
  mapStateToProps,
  mapDispatchToProps,
)(HelloWorldComponent)
