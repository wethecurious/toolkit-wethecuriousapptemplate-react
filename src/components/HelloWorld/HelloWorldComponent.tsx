import * as React from 'react'
import { Text, Heading, Button } from '@wethecurious/wtc-ui-library'

interface Props {
  demoMessage: string
  setDemoMessage: (message: string) => void
}

export const HelloWorldComponent: React.SFC<Props> = ({ demoMessage, setDemoMessage }) => {
  const handleClick = () => {
    console.log('click handled')
    setDemoMessage('This demo message is stored on the redux state.')
  }

  return (
    <>
      <Heading>We The Curious</Heading>
      <Text>This is a template to get started with React for a We The Curious project</Text>
      <Button
        onClick={handleClick}
        label='Set message'
      />
      <Text>{demoMessage}</Text>
    </>
  )
}