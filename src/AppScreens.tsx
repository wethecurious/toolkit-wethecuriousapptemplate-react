import * as React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Path } from './utils/pathsEnum'
import { Home } from './screens/Home'
import { Hello } from './screens/Hello'

// Put routes to each screen in the AppScreens component below
export const AppScreens: React.SFC = () => (
  <Switch>
    <Route exact={true} path={Path.Home} component={Home} />
    <Route exact={true} path={Path.Hello} component={Hello} />
  </Switch>
)
