import * as React from 'react'
import { AppScreens } from './AppScreens'
// tslint:disable-next-line
import styled from 'styled-components'

const AppContainer = styled.div`
  background-color: ${(props: Props) => props.theme.color.backgroundApp};
  min-height: 100vh;
`

interface Props {
  theme?: any
}

export const App: React.SFC<Props> = (props) => (
  <AppContainer>
      <AppScreens />
  </AppContainer>
)