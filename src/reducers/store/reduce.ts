import { createStore, combineReducers } from 'redux'
import { StoreState } from './storeState'
import { demoReducer } from '../demoReducer'

const appReducer = combineReducers<StoreState>({
  demoReducer,
  // add other reducers here to combine into one appReducer
})

export const store = createStore(
  appReducer,
  // @ts-ignore
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)
