export interface StoreState {
  demoReducer: DemoReducerState
}

export interface DemoReducerState {
  demoMessage: string
}