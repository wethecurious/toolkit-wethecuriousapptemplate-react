import { ActionTypes } from '../../store/utils/actionTypes'
import {
  SetDemoMessage,
  setDemoMessage,
} from '../'

describe('demoActions', () => {
  it('creates an action to set message', () => {
    const demoMessage: string = 'test message'
    const expectedAction: SetDemoMessage = {
      type: ActionTypes.SET_DEMO_MESSAGE,
      payload: {
        demoMessage,
      }
    }
    expect(setDemoMessage(demoMessage)).toEqual(expectedAction)
  })
})
