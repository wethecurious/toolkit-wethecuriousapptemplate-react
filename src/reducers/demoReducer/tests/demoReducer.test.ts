import { DemoReducerState } from '../../store/storeState'
import { demoReducer, SetDemoMessage } from '../'
import { DemoAction } from '../demoActions'
import { ActionTypes } from '../../store/utils/actionTypes'

describe('demoReducer', () => {
  it('sets the initial state', () => {
    const initialState: DemoReducerState = {
      demoMessage: '',
    }

    expect(demoReducer(undefined, {} as DemoAction)).toEqual(initialState)
  })

  it('correctly sets the demo message', () => {
    const demoMessage = 'test message'
    const action: SetDemoMessage = {
      type: ActionTypes.SET_DEMO_MESSAGE,
      payload: {
        demoMessage,
      }
    }
    const expectedState: DemoReducerState = {
      demoMessage,
    }

    expect(demoReducer(undefined, action)).toEqual(expectedState)
  })
})
