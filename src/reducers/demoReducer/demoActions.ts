import { ActionTypes } from '../store/utils/actionTypes'

export interface SetDemoMessage {
  type: ActionTypes.SET_DEMO_MESSAGE,
  payload: {
    demoMessage: string
  }
}
export const setDemoMessage = (demoMessage: string): SetDemoMessage => ({
  type: ActionTypes.SET_DEMO_MESSAGE,
  payload: {
    demoMessage,
  }
})

export type DemoAction = SetDemoMessage
