import { DemoReducerState } from '../store/storeState'
import { DemoAction } from './demoActions'
import { ActionTypes } from '../store/utils/actionTypes'

const initialState: DemoReducerState = {
  demoMessage: '',
}

export const demoReducer = (state: DemoReducerState = initialState, action: DemoAction) => {
  if (action.type === ActionTypes.SET_DEMO_MESSAGE) {
    return { ...state, demoMessage: action.payload.demoMessage }
  }

  return state
}
