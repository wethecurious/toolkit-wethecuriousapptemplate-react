import {
  SetDemoMessage,
  setDemoMessage,
} from './demoActions'
import { demoReducer } from './demoReducer'

export {
  SetDemoMessage,
  setDemoMessage,
  demoReducer,
}
