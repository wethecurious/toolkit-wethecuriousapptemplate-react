import { doubleNumber } from '../demoHelpers'

describe('doubleNumber', () => {
  it('returns an input number doubled', () => {
    expect(doubleNumber(5)).toBe(10)
  })
})
