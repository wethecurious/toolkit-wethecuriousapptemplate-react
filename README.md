# We The Curious react app template

This is a starter template for developers building apps for We The Curious. It uses React, Redux, React Router and Jest for testing. There is also linting rules configured that use the We The Curious prefered code style.

### Get started

This template has the [We The Curious component](https://www.npmjs.com/package/@wethecurious/wtc-ui-library) library pre installed. This has a hard dependency on the We The Curious Asset Server. The api for that can be found at https://assets.wethecurious.io/1/themes/latest or you can run it locally if any modifications need to be made.

This project uses yarn, if you want to use npm instead you will need to delete the yarn.lock file and run npm start from the root directory.

To run this template app locally use: `yarn start`

To run tests with Jest use: `yarn test`

To build this app to the dist folder use: `yarn build`

Tslint will run automatically when starting the app locally but you can also run it directly in the cli with `yarn test`